// console.log('Hello Edmar');

const trainer = {
	name: 'Edmar',
	age: 24,
	pokemon: ['Blaziken', 'Blastoise', 'Garchomp', 'Electivire'],
	friends: {hoenn: ['May', 'Max'], kanto: ['Brock', 'Misty']},
	talk: function() {
		console.log('Pikachu! I choose you!');
	}
};

console.log(trainer);
console.log('Result of dot notation:');
console.log(trainer.name);

console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);

console.log('Result of talk method');
trainer.talk();

function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level * 1.5;
	this.tackle = function(opponent) {
		console.log(this.name + ' tackled ' + opponent.name);

		console.log(opponent.name + ' is now reduced to ' + (opponent.health = opponent.health - this.attack));
		if (opponent.health <= 0){
			opponent.faint();
		}
	}
	this.faint = function() {
		console.log(this.name + ' has fainted');
	}
}

let combusken = new Pokemon('Combusken', 55);
console.log(combusken);

let wartortle = new Pokemon('Wartortle', 25);
console.log(wartortle);

let electabuzz = new Pokemon('Electabuzz', 60);
console.log(electabuzz);